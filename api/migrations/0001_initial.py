# Generated by Django 2.1.2 on 2018-10-03 07:21

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=60, verbose_name='Категория')),
                ('slug', models.SlugField(unique=True, verbose_name='URL')),
            ],
        ),
    ]
