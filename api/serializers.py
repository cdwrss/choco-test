from django.contrib.auth.models import User
from .models import Category, Post, Author, Comment, Grade
from rest_framework.test import APIRequestFactory
from rest_framework.request import Request
from django.core.validators import MaxValueValidator, MinValueValidator
from rest_framework import serializers

class CommentSerializer(serializers.ModelSerializer):
	author = serializers.ReadOnlyField(source='author.username')
	class Meta:
		model = Comment 
		fields = ('id', 'text', 'post','author', 'url')
		


class GradeSerializer(serializers.ModelSerializer):
	user = serializers.ReadOnlyField(source='user.username',read_only=True)
	rating = serializers.IntegerField(default=0,
        validators=[MaxValueValidator(10), MinValueValidator(0)])
	class Meta: 
		model = Grade
		fields = ('id',  'user','rating', 'post' )

	def create(self, validated_data):
		return Grade.objects.create(**validated_data)

class PostSerializer(serializers.ModelSerializer):
	image = serializers.ImageField(max_length=None, use_url=True)
	author = serializers.ReadOnlyField(source='author.username',read_only=True)
	created_at = serializers.ReadOnlyField()
	comments = CommentSerializer(many=True, read_only=True)
	grades = GradeSerializer(many=True, read_only=True)
	status = serializers.BooleanField()
	class Meta:
		model = Post
		fields = ('id','url','title', 'slug', 'text', 'image', 'category', 'status', 'author', 'created_at', 'comments', 'grades')
		lookup_field = 'slug'
		extra_kwargs = {
            'url': {'lookup_field': 'slug'}
        }

class AuthorSerializer(serializers.ModelSerializer):
	avatar = serializers.ImageField(max_length=None, use_url=True,required=False)
	rating = serializers.ReadOnlyField()
	class Meta:
		model = Author
		fields = ('rating', 'avatar')

class UserSerializer(serializers.ModelSerializer):

	author = AuthorSerializer()
	posts = PostSerializer(many=True,required=False)
	class Meta:
		model = User
		fields = ('id', 'username', 'author', 'posts')

	def update(self, instance, validated_data):
	    avatar = validated_data.get('author').get('avatar')
	    print(validated_data)
	    rating = validated_data.get('author').get('rating')
	    username = validated_data.get('username')
	    instance.author.avatar = avatar
	    instance.author.rating = rating
	    instance.author.save()
	    instance.username = username
	    instance.save()
	    return instance

class CategorySerializer(serializers.ModelSerializer):
	# posts = PostSerializer(many=True, read_only=True)
	class Meta:
		model = Category
		fields = ('id', 'url', 'name', 'slug')
		lookup_field = 'slug'
		extra_kwargs = {
            'url': {'lookup_field': 'slug'}
        }

class ProfileSerializer(serializers.Serializer):

	user = UserSerializer()