from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

from django.core.validators import MaxValueValidator, MinValueValidator

# Create your models here.

class Category(models.Model):
    name = models.CharField(max_length=60)
    slug = models.SlugField(max_length=50, unique=True)

    def __str__(self):
        return self.name

class Post(models.Model):
	title = models.CharField(max_length=128)
	slug = models.SlugField(max_length=50, unique=True)
	image = models.ImageField(upload_to="images/", default="images/none/none.jpg")
	text = models.TextField()
	category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='posts')
	created_at = models.DateTimeField(auto_now_add=True)
	status = models.BooleanField(default=False)
	updated_at = models.DateTimeField(auto_now=True)
	author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='posts')

class Author(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='author', primary_key=True)
	rating = models.IntegerField(null=True)
	avatar = models.ImageField(upload_to="images/avatars/", default="images/none/no-avatar.jpg", null=True)

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Author.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.author.save()

class Comment(models.Model):
	author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='comments')
	text =  models.TextField()
	updated_at = models.DateTimeField(auto_now=True)
	created_at = models.DateTimeField(auto_now_add=True)
	post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='comments')
	

class Grade(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='my_grades')
	post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='grades')
	rating = models.IntegerField(
        default=0,
        validators=[MaxValueValidator(10), MinValueValidator(0)]
    )
