from django.urls import path, include
from django.conf.urls import url, include

from rest_framework.urlpatterns import format_suffix_patterns

from .views import api_root, UserViewSet, CategoryViewSet, PostViewSet, ProfileView, CommentViewSet ,GradeViewSet

user_list = UserViewSet.as_view({
    'get': 'list',

})
user_detail = UserViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
})

category_list = CategoryViewSet.as_view({
    'get': 'list',
    'post': 'create'
})

category_detail = CategoryViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'delete': 'destroy'   
})

posts_list = PostViewSet.as_view({
    'get': 'list',
    'post': 'create'
})

post_detail = PostViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'delete': 'destroy'   
})

comments_list = CommentViewSet.as_view({
    'get': 'list',
    'post': 'create'
})

comments_detail = CommentViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'delete': 'destroy'   
})

grades_list = GradeViewSet.as_view({
    'get': 'list',
    'post': 'create'
})

grades_detail = GradeViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'delete': 'destroy'   
})


urlpatterns = format_suffix_patterns([
    url(r'^$', api_root),
    url(r'^users/$', user_list, name='user-list'),
    path('users/<int:pk>/', user_detail, name='user-detail'),
    url(r'^categories/$', category_list, name='category-list'),
    path('categories/<slug>/', category_detail, name='category-detail'),
    url(r'^posts/$', posts_list, name='post-list'),
    path('posts/<slug>/', post_detail, name='post-detail'),
    path('profile/', ProfileView.as_view(), name='profile'),
    path('comments/',comments_list, name='comment-list' ),
    path('comments/<int:pk>/',comments_detail, name='comment-detail' ),
    path('grades/', grades_list, name='grades-list'),
    path('grades/<int:pk>/', grades_detail, name='grades-detail'),
])
