from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.http import Http404
from django.db.models import Q

from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework import permissions
from rest_framework import generics
from rest_framework import viewsets
from rest_framework.authentication import TokenAuthentication
from rest_auth.registration.views import LoginView
from rest_framework import permissions
from rest_framework.views import APIView
from rest_framework.pagination import LimitOffsetPagination

from .serializers import UserSerializer, CategorySerializer, PostSerializer, CommentSerializer, GradeSerializer, ProfileSerializer

from django.contrib.auth.models import User
from .models import Category, Post, Comment, Grade

from .permissions import IsOwnerOrReadOnly

# Create your views here.
class LoginViewCustom(LoginView):
    authentication_classes = (TokenAuthentication,)


@api_view(['GET'])
def api_root(request, format=None):
    return Response({
        'users': reverse('user-list', request=request, format=format),
        'categories': reverse('category-list', request=request, format=format),
        'posts': reverse('post-list', request=request, format=format),
        'profile': reverse('profile', request=request, format=format),
  	})


class IsSuperUser(permissions.BasePermission):

    def has_permission(self, request, view):
        return request.user and request.user.is_superuser

class IsUser(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        if request.user:
            if request.user.is_superuser:
                return True
            else:
                return obj == request.user
        else:
            return False

class UserViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list` and `detail` actions.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (permissions.IsAdminUser,)

    def retrieve(self, request, pk=None):
        queryset = User.objects.all().select_related('author')
        user = get_object_or_404(queryset, pk=pk)
        serializer = UserSerializer(user, context= {'request':request})
        return Response(serializer.data)



class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    lookup_field = 'slug'
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)


class ProfileView(APIView):

    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        if not request.user.is_anonymous:
            print(request.user.id)
            user = User.objects.get(pk=request.user.id)
            serializer = UserSerializer(user, context={'request': request})
            return Response(serializer.data)
        else:
            raise Http404

    def put(self, request, format=None):
        if not request.user.is_anonymous:
            user = User.objects.get(pk=request.user.id)
            serializer = ProfileSerializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            user.username = serializer.validated_data.get('user').get('username')
            user.author.avatar = serializer.validated_data.get('user').get('author').get('avatar')
            user.save()
            return Response(serializer.data)
        else:
            raise Http404



class PostViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    lookup_field = 'slug'
    pagination_class = LimitOffsetPagination
    permission_classes = (IsOwnerOrReadOnly, permissions.IsAuthenticatedOrReadOnly,)

    def list(self, request):
        context={'request': request}
        paginator = LimitOffsetPagination()
        paginator.page_size = 10
        permission_classes = (permissions.IsAuthenticatedOrReadOnly, )
        queryset = Post.objects.filter(status=True).all()
        result_page = paginator.paginate_queryset(queryset, request)
        serializer = PostSerializer(queryset, many=True,context=context)
        return paginator.get_paginated_response(serializer.data)

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)

class CommentViewSet(viewsets.ModelViewSet):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly)

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)

class GradeViewSet(viewsets.ModelViewSet):
    queryset = Grade.objects.all()
    serializer_class = GradeSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly)
    

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)